namespace Cercle;
class Circle
{
    public Point centre;
    public double rayon;

    public Circle(Point C, double r)
    {
        centre = C; rayon = r;
    }

    public void getPermimeter()
    {
        Console.WriteLine("Permimètre :" + (2 * rayon * Math.PI));
    }

    public void getSurface()
    {
        Console.WriteLine("Surface :" + (3.14 * Math.Pow(rayon, 2)));
    }
    public void IsInclude(Point p)
    {
        double dist = Math.Sqrt(Math.Pow(p.x - centre.x, 2) + Math.Pow(p.y - centre.y, 2));
        if (dist <= rayon)
            Console.WriteLine("Le point est dans le cercle.");
        else Console.WriteLine("le point n'est pas dans le cercle. ");
    }
}