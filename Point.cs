namespace Cercle;

class Point
{
    public double y;
    public double x;


    public Point(double X, double Y)
    {
        x = X; y = Y;
    }

    public void Display()
    {
        Console.WriteLine("Point({0},{1})", x, y);

    }

}