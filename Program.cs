﻿
namespace Cercle;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Entrez les coordonnées du centre de votre cercle (x puis y) :");
        double x = Convert.ToDouble(Console.ReadLine());
        double y = Convert.ToDouble(Console.ReadLine());
        
        
        Point center = new Point(x, y);
        center.Display();
        Console.WriteLine("\nEntrez le rayon du cercle : ");
        double r = Convert.ToDouble(Console.ReadLine());

        Circle circle = new Circle(center, r);

        circle.getPermimeter();
        circle.getSurface();


        Console.WriteLine("Entrez les coordonnées du point dont vous voulez vérifier l'appartenance au cercle (x puis y)");
        double x1 = Convert.ToDouble(Console.ReadLine());
        double y1 = Convert.ToDouble(Console.ReadLine());
        Point point = new Point(x1, y1);
        point.Display();

        circle.IsInclude(point);
    }


}